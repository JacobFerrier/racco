package testing

import (
	"fmt"
	"math/rand"
	"os"

	"bitbucket.org/JacobFerrier/racco/functions"
)

const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

func createRandomElement(maxLength int) string {
	var t string
	for i := 0; i < rand.Intn(maxLength)+1; i++ {
		t += string(characters[rand.Intn(len(characters))])
	}

	return t
}

// CreateRandomDatasets to be used for testing
func CreateRandomDatasets(destination string, numSets int, elementCount int, elementMaxLength int) {
	for i := 0; i < numSets; i++ {
		fmt.Println("Creating " + destination + fmt.Sprintf("%04d", i) + ".dataset")
		f, err := os.Create(destination + fmt.Sprintf("%04d", i) + ".dataset")
		functions.Check(err)
		defer f.Close()

		for j := 0; j < elementCount; j++ {
			f.WriteString(createRandomElement(elementMaxLength) + "\t" + "random" + "\t" + "junk" + "\n")
		}

		f.Sync()
	}
}
