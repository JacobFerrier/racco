package indexing

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"

	"bitbucket.org/JacobFerrier/racco/functions"
)

func createProfile(destination string, datasets string, file os.FileInfo, index map[string]int) {
	var indicesOfZeroes []int
	for _, b := range bytes.Split(functions.GetNecessaryInfo(datasets+file.Name()), []byte(" ")) {
		if len(b) > 0 {
			indicesOfZeroes = append(indicesOfZeroes, index[string(b)])
		}
	}

	sortedIndicesOfZeroes := functions.Sort(indicesOfZeroes)
	err := ioutil.WriteFile(destination+strings.Split(file.Name(), ".")[0]+".profile", createProfileBytes(sortedIndicesOfZeroes), 0644)
	functions.Check(err)
}

func createProfileBytes(indices []int) []byte {
	var s string
	for i := 0; i < len(indices); i++ {
		s += strconv.Itoa(indices[i]) + " "
	}

	return []byte(s)
}

//returns a string of 1s or 0s depending on index hits in main index
//use:
//profileString := createProfileString(sortedIndicesOfZeroes, len(index))
func createProfileString(indices []int, length int) string {
	var s string
	for i := 0; i < length; i++ {
		if functions.ContainsIntSlice(i, indices) {
			s += "1"
		} else {
			s += "0"
		}
	}

	return s
}

func createIndex(destination string, datasets string, files []os.FileInfo) map[string]int {
	var temp [][]byte
	for _, file := range files {
		for _, b := range bytes.Split(functions.GetNecessaryInfo(datasets+file.Name()), []byte(" ")) {
			if len(b) > 0 {
				if !contains(b, temp) {
					temp = append(temp, b)
				}
			}
		}
	}

	return makeMap(temp)
}

func contains(itemToCheck []byte, sliceToCheck [][]byte) bool {
	var returnVal = false
	if len(sliceToCheck) != 0 {
		for i := 0; i < len(sliceToCheck); i++ {
			if string(sliceToCheck[i]) == string(itemToCheck) {
				returnVal = true
			}
		}
	}

	return returnVal
}

func makeMap(slice [][]byte) map[string]int {
	var m = make(map[string]int)
	for i := 0; i < len(slice); i++ {
		m[string(slice[i])] = i
	}

	return m
}

// ProfileDatasets based on directory of datasets provided using the indexing method of profiling
func ProfileDatasets(destination string, datasets string) {
	files, err := ioutil.ReadDir(datasets)
	functions.Check(err)

	index := createIndex(destination, datasets, files)

	for _, file := range files {
		if !strings.HasPrefix(file.Name(), ".") {
			fmt.Println("Creating " + destination + strings.Split(file.Name(), ".")[0] + ".profile")
			createProfile(destination, datasets, file, index)
		}
	}
}
