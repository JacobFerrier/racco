package reduction

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"bitbucket.org/JacobFerrier/racco/functions"
)

func createProfile(destination string, datasets string, file os.FileInfo) {
	err := ioutil.WriteFile(destination+strings.Split(file.Name(), ".")[0]+".profile", functions.GetNecessaryInfo(datasets+file.Name()), 0644)
	functions.Check(err)
}

// ProfileDatasets based on directory of datasets provided using the reduction method of profiling
func ProfileDatasets(destination string, datasets string) {
	files, err := ioutil.ReadDir(datasets)
	functions.Check(err)

	for _, file := range files {
		if !strings.HasPrefix(file.Name(), ".") {
			fmt.Println("Creating " + destination + strings.Split(file.Name(), ".")[0] + ".profile")
			createProfile(destination, datasets, file)
		}
	}
}
