package functions

import (
	"bytes"
	"io/ioutil"
)

// Check existence of error, panics if error found
func Check(e error) {
	if e != nil {
		panic(e)
	}
}

// Inter unpacks interface into slice
func Inter(a ...interface{}) []interface{} {
	return a
}

// GetNecessaryInfo from file name provided, returns byte slice. This currently serves as a simple, temporary alternative to intelligent parsing
func GetNecessaryInfo(file string) []byte {
	contents, err := ioutil.ReadFile(file)
	Check(err)

	out := []byte{}
	for _, v := range bytes.Split(contents, []byte("\n")) {
		if len(v) > 0 {
			out = append(out, bytes.Split(v, []byte("\t"))[0]...)
			out = append(out, []byte(" ")...)
		}
	}

	return out
}

// Sort int slice using bubblesort
func Sort(slice []int) []int {
	var temp int
	for i := 0; i < len(slice)-1; i++ {
		for j := 0; j < len(slice)-i-1; j++ {
			if slice[j] > slice[j+1] {
				temp = slice[j]
				slice[j] = slice[j+1]
				slice[j+1] = temp
			}
		}
	}

	return slice
}

// ContainsIntSlice checks if provided slice contains an int to be searched
func ContainsIntSlice(itemToCheck int, sliceToCheck []int) bool {
	var returnVal = false
	for i := 0; i < len(sliceToCheck); i++ {
		if itemToCheck == sliceToCheck[i] {
			returnVal = true
		}
	}

	return returnVal
}
