package main

import (
	"fmt"
	"io/ioutil"
	"math/bits"

	"bitbucket.org/JacobFerrier/racco/pkg/common"
	"bitbucket.org/JacobFerrier/racco/pkg/dataset"

	"bitbucket.org/JacobFerrier/racco/pkg/bloomfilter"
	"bitbucket.org/JacobFerrier/racco/pkg/compare"
)

func main() {

	out := ""
	for fpr := 0.01; fpr < 1.01; fpr += 0.01 {
		ds1 := dataset.ConstructFromFile("/Users/jferrier/go/src/bitbucket.org/JacobFerrier/racco/data/datasets/testing/0000.dataset")
		bf1 := bloomfilter.ConstructFromDataset(ds1, fpr)
		ds2 := dataset.ConstructFromFile("/Users/jferrier/go/src/bitbucket.org/JacobFerrier/racco/data/datasets/testing/0015.dataset")
		bf2 := bloomfilter.ConstructFromDataset(ds2, fpr)

		//fmt.Printf("%08b\n", bf1.GetBytes())
		//fmt.Printf("%08b\n", bf2.GetBytes())

		max := bf1.GetNumBitsInFilter()
		min := bf2.GetNumBitsInFilter()
		if bf1.GetNumBitsInFilter() < bf2.GetNumBitsInFilter() {
			max = bf2.GetNumBitsInFilter()
			min = bf1.GetNumBitsInFilter()
		}

		//fmt.Printf("Min: %v, Max: %v\n", min, max)

		count := 0
		for i := 0; i < min/8; i++ {
			//fmt.Printf("Comparing %08b and %08b\n", bf1.GetBytes()[i], bf2.GetBytes()[i])
			//fmt.Printf("Comparison: %08b\n", bf1.GetBytes()[i]&bf2.GetBytes()[i])
			//fmt.Printf("Ones count: %v\n", bits.OnesCount(uint(bf1.GetBytes()[i]&bf2.GetBytes()[i])))
			count += bits.OnesCount(uint(bf1.GetBytes()[i] & bf2.GetBytes()[i]))
		}
		/*
			fmt.Printf("Using false positive rate: %f\n", fpr)
			fmt.Printf("Number of ones in filter 1: %v\n", bf1.GetNumOnesInFilter())
			fmt.Printf("Number of bits in filter 1: %v\n", bf1.GetNumBitsInFilter())
			fmt.Printf("Ratio of ones to bits in filter 1: %v\n", float32(bf1.GetNumOnesInFilter())/float32(bf1.GetNumBitsInFilter()))
			fmt.Printf("Old ds-ds similarity score: %v\n", compare.DatasetToDataset(ds1, ds2, false))
			fmt.Printf("Old ds-bf similarity score: %v\n", compare.DatasetToBloomFilter(ds1, bf2, false))
			fmt.Printf("Old bf-bf similarity score: %v\n", compare.BloomFilterToBloomFilter(bf1, bf2, false))
			fmt.Printf("New bf-bf similarity score: %v\n\n", float32(count)/float32(max))
		*/
		out += fmt.Sprintf("%f\t%v\t%v\t%v\t%v\t%v\t%v\t%v\t%v\t%v\t%v\n", fpr, bf1.GetNumOnesInFilter(), bf1.GetNumBitsInFilter(), float32(bf1.GetNumOnesInFilter())/float32(bf1.GetNumBitsInFilter()), bf2.GetNumOnesInFilter(), bf2.GetNumBitsInFilter(), float32(bf2.GetNumOnesInFilter())/float32(bf2.GetNumBitsInFilter()), compare.DatasetToDataset(ds1, ds2, false), compare.DatasetToBloomFilter(ds1, bf2, false), compare.BloomFilterToBloomFilter(bf1, bf2, false), float32(count)/float32(max))
	}
	err := ioutil.WriteFile("/Users/jferrier/Desktop/bloomfilter_testing.txt", []byte(out), 0644)
	common.Check(err)

	/*
		mainDir := common.GetMainDir()

		fmt.Print("\n-----\nCreating testing datsets\n-----\n\n")
		create.TestDatasets("testing", 100, 20000, 7, true)

		fmt.Print("\n-----\nCreating bloomfilter profiles for testing datasets\n-----\n\n")
		create.BloomFilterProfiles("testing", 0.20, true)

		var score float32
		var which string

		fmt.Print("\n-----\nTesting comparison between a dataset of testing and a dataset of testing\n-----\n\n")
		score = compare.DatasetToDataset(dataset.ConstructFromFile(mainDir+"/data/datasets/testing/0000.dataset"), dataset.ConstructFromFile(mainDir+"/data/datasets/testing/0015.dataset"))
		fmt.Println("Score: " + fmt.Sprintf("%f", score))

		fmt.Print("\n-----\nTesting comparison between a dataset of testing and a bloomfilter of testing\n-----\n\n")
		score = compare.DatasetToBloomFilter(dataset.ConstructFromFile(mainDir+"/data/datasets/testing/0000.dataset"), bloomfilter.ConstructFromFile(mainDir+"/data/profiles/testing/0015.profile"))
		fmt.Println("Score: " + fmt.Sprintf("%f", score))

		fmt.Print("\n-----\nTesting comparison between a bloomfilter of testing and another bloomfilter of testing\n-----\n\n")
		score = compare.BloomFilterToBloomFilter(bloomfilter.ConstructFromFile(mainDir+"/data/profiles/testing/0000.profile"), bloomfilter.ConstructFromFile(mainDir+"/data/profiles/testing/0015.profile"))
		fmt.Println("Score: " + fmt.Sprintf("%f", score))

		fmt.Print("\n-----\nTesting comparison between a dataset of testing and all bloomfilters of testing\n-----\n\n")
		score, which = compare.DatasetToBloomFilters(dataset.ConstructFromFile(mainDir+"/data/datasets/testing/0000.dataset"), "testing")
		fmt.Println("Highest non-trivial score: " + fmt.Sprintf("%f", score))
		fmt.Println("Highest scorer: " + which)

		fmt.Print("\n-----\nTesting comparison between a bloomfilter of testing and all datasets of testing\n-----\n\n")
		score, which = compare.BloomFilterToDatasets(bloomfilter.ConstructFromFile(mainDir+"/data/profiles/testing/0000.profile"), "testing")
		fmt.Println("Highest non-trivial score: " + fmt.Sprintf("%f", score))
		fmt.Println("Highest scorer: " + which)

		fmt.Print("\n-----\nTesting benchmark for bloomfilter construction\n-----\n\n")
		bloomfilter.BenchmarkSingle(100000000, 0.01, 10, 10)
	*/
}
