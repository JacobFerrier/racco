package compare

import (
	"fmt"
	"log"
	"math/bits"

	"bitbucket.org/JacobFerrier/racco/pkg/bloomfilter"
	"bitbucket.org/JacobFerrier/racco/pkg/dataset"
)

// DatasetToDataset compares a Dataset to a Dataset taking in both as filenames, the method returns a score as the number of hits
// (times an element of the smaller Dataset was in the larger Dataset) divided by the number of possible hits (total number of elements in the larger Dataset)
func DatasetToDataset(d1 dataset.Dataset, d2 dataset.Dataset, verbose bool) float32 {
	if verbose {
		fmt.Println("Comparing " + d1.GetFullName() + " to " + d2.GetFullName())
	}

	max := d1
	min := d2
	if d1.GetNumElements() < d2.GetNumElements() {
		max = d2
		min = d1
	}

	var hits int
	for key := range min.GetElements() {
		if _, ok := max.GetElements()[key]; ok {
			hits++
		}
	}

	return float32(hits) / float32(max.GetNumElements())
}

// DatasetToBloomFilter compares a Dataset to a BloomFilter profile taking in both as filenames, the method returns a score as the number of hits
// (times an element of the Dataset was probably in the BloomFilter) divided by the number of possible hits (total number of elements in the Dataset)
func DatasetToBloomFilter(d dataset.Dataset, bf bloomfilter.BloomFilter, verbose bool) float32 {
	if verbose {
		fmt.Println("Comparing " + d.GetFullName() + " to " + bf.GetFullName())
	}

	var hits int
	for e := range d.GetElements() {
		if bf.ProbablyExists(e) {
			hits++
		}
	}

	return float32(hits) / float32(d.GetNumElements())
}

// BloomFilterToBloomFilter compares a BloomFilter profile to another BloomFilter profile taking in both as filenames, the method returns a score as the number of hits
// (times the bits in both bitmaps directly aligned) divided by the number of possible hits (length of the largest bitmap)
func BloomFilterToBloomFilter(bf1 bloomfilter.BloomFilter, bf2 bloomfilter.BloomFilter, verbose bool) float32 {
	if verbose {
		fmt.Println("Comparing " + bf1.GetFullName() + " to " + bf2.GetFullName())
	}

	max := bf1.GetNumBitsInFilter()
	min := bf2.GetNumBitsInFilter()
	if bf1.GetNumBitsInFilter() < bf2.GetNumBitsInFilter() {
		max = bf2.GetNumBitsInFilter()
		min = bf1.GetNumBitsInFilter()
	}

	//fmt.Printf("min: %v\nmax: %v\n", min, max)

	var hits int
	//min or max are not truly the sizes of the bytes of the filter, instead it is the number of bits containing real data (excluding the extra end bits used for rounding up to a byte), thus this is good for scoring
	for i := 0; i < min; i++ {
		if bf1.GetBitValue(i) == bf2.GetBitValue(i) {
			hits++
		}
	}

	return float32(hits) / float32(max)
}

// OneHammingDistance function computes the Hamming distance between two bloomfilters but with only taking into account
// when 1's align in the two filters and specifically not when 0's align
func OneHammingDistance(bf1 bloomfilter.BloomFilter, bf2 bloomfilter.BloomFilter) float64 {
	if bf1.GetNumBitsInFilter() != bf2.GetNumBitsInFilter() {
		log.Fatal("Tried to compare bloomfilters of different sizes")
	}

	bytes1 := bf1.GetBytes()
	bytes2 := bf2.GetBytes()

	oneCount := 0
	for i := 0; i < len(bytes1); i++ {
		result := bytes1[i] & bytes2[i]
		oneCount += bits.OnesCount(uint(result))
	}

	return float64(oneCount) / float64(bf1.GetNumBitsInFilter())
}

// JaccardSimilarity function computes the Jaccard Similarity between two bloomfilters
func JaccardSimilarity(bf1 bloomfilter.BloomFilter, bf2 bloomfilter.BloomFilter) float64 {
	if bf1.GetNumBitsInFilter() != bf2.GetNumBitsInFilter() {
		log.Fatal("Tried to compare bloomfilters of different sizes")
	}

	bytes1 := bf1.GetBytes()
	bytes2 := bf2.GetBytes()

	andOneCount, orOneCount := 0, 0
	for i := 0; i < len(bytes1); i++ {
		andResult := bytes1[i] & bytes2[i]
		orResult := bytes1[i] | bytes2[i]

		andOneCount += bits.OnesCount(uint(andResult))
		orOneCount += bits.OnesCount(uint(orResult))
	}

	return float64(andOneCount) / float64(orOneCount)
}

// JaccardSimilarityBytemaps function computes the Jaccard Similarity between two bytemaps
func JaccardSimilarityBytemaps(bytes1 bloomfilter.Bytemap, bytes2 bloomfilter.Bytemap) float64 {
	if len(bytes1) != len(bytes2) {
		log.Fatal("Tried to compare bloomfilters of different sizes")
	}

	andOneCount, orOneCount := 0, 0
	for i := 0; i < len(bytes1); i++ {
		andResult := bytes1[i] & bytes2[i]
		orResult := bytes1[i] | bytes2[i]

		andOneCount += bits.OnesCount(uint(andResult))
		orOneCount += bits.OnesCount(uint(orResult))
	}

	return float64(andOneCount) / float64(orOneCount)
}
