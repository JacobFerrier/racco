package compare

import (
	"fmt"
	"io/ioutil"
	"strings"

	"bitbucket.org/JacobFerrier/racco/pkg/bloomfilter"

	"bitbucket.org/JacobFerrier/racco/pkg/dataset"

	"bitbucket.org/JacobFerrier/racco/pkg/common"
)

// DatasetToBloomFilters compares a Dataset to all BloomFilter profiles in a directory to find the best match, utalizing the DatasetToBloomFilter method
// for individual BloomFilters in directory, returns the highest scoring (most smiliar BloomFilter) without the trivial comparison (comparing a Dataset
// against its own BloomFilter)
func DatasetToBloomFilters(d dataset.Dataset, profileSetName string, verbose bool) (float32, string) {
	fmt.Println("Comparing " + d.GetFullName() + " to " + profileSetName + " profiles")

	files, err := ioutil.ReadDir(common.GetMainDir() + "/data/profiles/" + profileSetName + "/")
	common.Check(err)

	maxScore := float32(0)
	profileWithMaxScore := ""
	for _, file := range files {
		if !strings.HasPrefix(file.Name(), ".") {
			bf := bloomfilter.ConstructFromFile(common.GetMainDir() + "/data/profiles/" + profileSetName + "/" + file.Name())
			score := DatasetToBloomFilter(d, bf, verbose)
			if score > maxScore && d.GetFullName() != strings.ReplaceAll(bf.GetFullName(), ".profile", ".dataset") {
				maxScore = score
				profileWithMaxScore = profileSetName + "/" + file.Name()
			}
		}
	}

	return maxScore, profileWithMaxScore
}

// BloomFilterToDatasets compares a BloomFilter to all Datasets in a directory to find the best match, utalizing the DatasetToBloomFilter method
// for individual Datasets in directory, returns the highest scoring (most smiliar Dataset) without the trivial comparison (comparing a Dataset
// against its own BloomFilter)
func BloomFilterToDatasets(bf bloomfilter.BloomFilter, datasetSetName string, verbose bool) (float32, string) {
	fmt.Println("Comparing " + bf.GetFullName() + " to " + datasetSetName + " datasets")

	files, err := ioutil.ReadDir(common.GetMainDir() + "/data/datasets/" + datasetSetName + "/")
	common.Check(err)

	maxScore := float32(0)
	datasetWithMaxScore := ""
	for _, file := range files {
		if !strings.HasPrefix(file.Name(), ".") {
			d := dataset.ConstructFromFile(common.GetMainDir() + "/data/datasets/" + datasetSetName + "/" + file.Name())
			score := DatasetToBloomFilter(d, bf, verbose)
			if score > maxScore && bf.GetFullName() != strings.ReplaceAll(d.GetFullName(), ".dataset", ".profile") {
				maxScore = score
				datasetWithMaxScore = datasetSetName + "/" + file.Name()
			}
		}
	}

	return maxScore, datasetWithMaxScore
}
