package dataset

import (
	"bytes"
	"io/ioutil"
	"regexp"
	"strings"

	"bitbucket.org/JacobFerrier/racco/pkg/common"
)

////
// Dataset construction methods, can either build a new Dataset or parse in a already constructed Dataset from a file
////

// Dataset object
type Dataset struct {
	elements    map[string]struct{}
	numElements int
	group       string
	name        string
}

// NewDataset builds a new Dataset object given a string splice, a group and a name
func NewDataset(elements []string, group string, name string) Dataset {
	e := make(map[string]struct{})
	for _, s := range elements {
		e[s] = struct{}{}
	}

	return Dataset{e, len(e), group, name}
}

// ConstructFromFile takes the filename of a Dataset and parses it in as a Dataset object
func ConstructFromFile(filename string) Dataset {
	contents, err := ioutil.ReadFile(filename)
	common.Check(err)

	e := make(map[string]struct{})
	for _, b := range bytes.Split(contents, []byte("\n")) {
		if len(b) > 0 {
			e[string(bytes.Split(b, []byte("\t"))[0])] = struct{}{}
		}
	}

	fullName := strings.SplitAfter(filename, "/data/datasets/")[1]
	r, _ := regexp.Compile("([0-9]+).dataset")
	name := r.FindString(fullName)
	group := strings.ReplaceAll(fullName, name, "")[:len(strings.ReplaceAll(fullName, name, ""))-1]

	return Dataset{e, len(e), group, name}
}

////
// Getters
////

// GetElements public getter for elements
func (d *Dataset) GetElements() map[string]struct{} {
	return d.elements
}

// GetNumElements public getter for numElements
func (d *Dataset) GetNumElements() int {
	return d.numElements
}

// GetGroup public getter for group
func (d *Dataset) GetGroup() string {
	return d.group
}

// GetName public getter for name
func (d *Dataset) GetName() string {
	return d.name
}

// GetFullName returns full name of Dataset
func (d *Dataset) GetFullName() string {
	return d.group + "/" + d.name
}

////
// Dataset operation helper methods
////

// addElement adds a string element into a Dataset object's elements
func (d *Dataset) addElement(s string) {
	d.elements[s] = struct{}{}
}

func (d *Dataset) elementsToWriteByteSlice() []byte {
	var byteSlice []byte
	for s := range d.elements {
		byteSlice = append(byteSlice, (s + "\n")...)
	}

	return byteSlice
}

////
// Dataset file writing methods
////

// WriteToFile writes out a Dataset to a file derived from its attributes
func (d *Dataset) WriteToFile() {
	err := ioutil.WriteFile(common.GetMainDir()+"/data/datasets"+"/"+d.group+"/"+d.name, d.elementsToWriteByteSlice(), 0644)
	common.Check(err)
}
