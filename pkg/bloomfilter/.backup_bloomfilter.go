
////
// BloomFilter construction methods, can either build a new BloomFilter or parse in a already constructed BloomFilter from a file
////

/*

// BloomFilter object
type BloomFilter struct {
	bitmap           map[int]uint8
	numItemsInFilter int
	fpr              float64
	numBitsInFilter  int
	numHashFunctions int
}

// NewBloomFilter builds a new BloomFilter object given the number of items and false positive rate (fpr)
func NewBloomFilter(numItemsInFilter int, fpr float64) BloomFilter {
	b := make(map[int]uint8)
	numBitsInFilter := calcNumBitsInBloomFilter(numItemsInFilter, fpr)
	numHashFunctions := calcNumHashFunctions(numBitsInFilter, numItemsInFilter)

	for i := 0; i < numBitsInFilter; i++ {
		b[i] = 0
	}

	return BloomFilter{b, numItemsInFilter, fpr, numBitsInFilter, numHashFunctions}
}

// ConstructFromFile takes the filename of a BloomFilter and parses it in as a BloomFilter object
func ConstructFromFile(filename string) BloomFilter {
	contents, err := ioutil.ReadFile(filename)
	common.Check(err)

	numItemsInFilter, err := strconv.Atoi(strings.Split(string(bytes.Split(contents, []byte("\n"))[0]), ": ")[1])
	common.Check(err)
	fpr, err := strconv.ParseFloat(strings.Split(string(bytes.Split(contents, []byte("\n"))[1]), ": ")[1], 64)
	common.Check(err)
	numBitsInFilter, err := strconv.Atoi(strings.Split(string(bytes.Split(contents, []byte("\n"))[2]), ": ")[1])
	common.Check(err)
	numHashFunctions, err := strconv.Atoi(strings.Split(string(bytes.Split(contents, []byte("\n"))[3]), ": ")[1])
	common.Check(err)
	bitString := string(bytes.Split(contents, []byte("\n"))[5])

	b := make(map[int]uint8)

	for i := 0; i < len(bitString); i++ {
		b[i] = uint8(common.Inter(strconv.ParseUint(string(bitString[i]), 10, 8))[0].(uint64))
	}

	return BloomFilter{b, numItemsInFilter, fpr, numBitsInFilter, numHashFunctions}
}

// ConstructFromDataset takes in a Dataset and false positive rate (fpr) and returns a BloomFilter from it
func ConstructFromDataset(d dataset.Dataset, fpr float64) BloomFilter {
	bf := NewBloomFilter(d.GetNumElements(), fpr)

	for e := range d.GetElements() {
		bf.AddToBloomFilter(e)
	}

	return bf
}

// ConstructFromFile takes the filename of a BloomFilter and parses it in as a BloomFilter object
func ConstructFromFile(filename string) BloomFilter {
	contents, err := ioutil.ReadFile(filename)
	common.Check(err)

	numItemsInFilter, err := strconv.Atoi(strings.Split(string(bytes.Split(contents, []byte("\n"))[0]), ": ")[1])
	common.Check(err)
	fpr, err := strconv.ParseFloat(strings.Split(string(bytes.Split(contents, []byte("\n"))[1]), ": ")[1], 64)
	common.Check(err)
	numBitsInFilter, err := strconv.Atoi(strings.Split(string(bytes.Split(contents, []byte("\n"))[2]), ": ")[1])
	common.Check(err)
	numHashFunctions, err := strconv.Atoi(strings.Split(string(bytes.Split(contents, []byte("\n"))[3]), ": ")[1])
	common.Check(err)
	bitString := string(bytes.Split(contents, []byte("\n"))[5])

	b := make(map[int]uint8)

	for i := 0; i < len(bitString); i++ {
		b[i] = uint8(common.Inter(strconv.ParseUint(string(bitString[i]), 10, 8))[0].(uint64))
	}

	return BloomFilter{b, numItemsInFilter, fpr, numBitsInFilter, numHashFunctions}
}

////
// Getters
////

// GetBitmap public getter for bitmap
func (bf *BloomFilter) GetBitmap() map[int]uint8 {
	return bf.bitmap
}

// GetNumItemsInFilter public getter for numItemsInFilter
func (bf *BloomFilter) GetNumItemsInFilter() int {
	return bf.numItemsInFilter
}

// Getfpr public getter for fpr
func (bf *BloomFilter) Getfpr() float64 {
	return bf.fpr
}

// GetNumBitsInFilter public getter for numBitsInFilter
func (bf *BloomFilter) GetNumBitsInFilter() int {
	return bf.numBitsInFilter
}

// GetNumHashFunctions public getter for numHashFunctions
func (bf *BloomFilter) GetNumHashFunctions() int {
	return bf.numHashFunctions
}

////
// BloomFilter operation helper methods
////

// calcNumBitsInBloomFilter calculates the appropriate number of bits for a filter
func calcNumBitsInBloomFilter(numItemsInFilter int, fpr float64) int {
	return int(math.Ceil((-1.0 * float64(numItemsInFilter) * math.Log(fpr)) / (math.Pow(math.Log(2), 2))))
}

// calcNumHashFunctions calculates the appropriate number of hash functions for a filter
func calcNumHashFunctions(numBitsInFilter int, numItemsInFilter int) int {
	return int(math.Round(float64(numBitsInFilter/numItemsInFilter) * math.Log(2)))
}

// AddToBloomFilter a passed in string
func (bf *BloomFilter) AddToBloomFilter(s string) {
	h1, h2 := murmur3.Sum128([]byte(s))
	for i := 0; i < bf.numHashFunctions; i++ {
		h1 += uint64(bf.numHashFunctions) * h2
		bf.bitmap[int(h1%uint64(bf.numBitsInFilter))] = 1
	}
}

// ProbablyExists tests if a string passed in probably exists in the filter
func (bf *BloomFilter) ProbablyExists(s string) bool {
	returnVal := true
	h1, h2 := murmur3.Sum128([]byte(s))
	for i := 0; i < bf.numHashFunctions; i++ {
		h1 += uint64(bf.numHashFunctions) * h2
		if bf.bitmap[int(h1%uint64(bf.numBitsInFilter))] == 0 {
			returnVal = false
		}
	}

	return returnVal
}

////
// BloomFilter file writing methods
////

// WriteToFile writes out a BloomFilter to a file specified by filename
func (bf *BloomFilter) WriteToFile(filename string) {
	out := ("#number of items in filter: " + strconv.Itoa(bf.numItemsInFilter)) + "\n" +
		("#false positive rate of filter: " + fmt.Sprintf("%g", bf.fpr)) + "\n" +
		("#number of bits in filter: " + strconv.Itoa(bf.numBitsInFilter)) + "\n" +
		("#number of hash functions for filter: " + strconv.Itoa(bf.numHashFunctions)) + "\n" +
		("#bitmap:") + "\n"

	for i := 0; i < bf.numBitsInFilter; i++ {
		out += strconv.FormatUint(uint64(bf.bitmap[i]), 10)
	}

	err := ioutil.WriteFile(filename, []byte(out), 0644)
	common.Check(err)
}

*/
