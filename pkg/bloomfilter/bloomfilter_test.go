package bloomfilter

import (
	"os"
	"testing"

	"bitbucket.org/JacobFerrier/racco/pkg/common"
	"github.com/spaolacci/murmur3"
)

func TestGetBitmap(t *testing.T) {
	bf := NewBloomFilter(100, 0.05, "temp", "temp")

	got := bf.GetBytes()

	b := make(map[int]uint8)
	for i := 0; i < bf.GetNumBitsInFilter(); i++ {
		b[i] = 0
	}

	if len(got) != len(b) {
		t.Errorf("expected the length of got := bf.GetBitmap() to equal the test bitmap, it does not")
	}

	for j := 0; j < len(got); j++ {
		if got[j] != b[j] {
			t.Errorf("expected the value of index %d of got := bf.GetBitmap() to equal the value of index %d of the test bitmap, it does not", j, j)
		}
	}

}

func TestGetNumItemsInFilter(t *testing.T) {
	bf := NewBloomFilter(100, 0.05, "temp", "temp")

	got := bf.GetNumItemsInFilter()
	if got != 100 {
		t.Errorf("bf.GetNumItemsInFilter() = %d; want 100", got)
	}
}

func TestGetfpr(t *testing.T) {
	bf := NewBloomFilter(100, 0.05, "temp", "temp")

	got := bf.Getfpr()
	if got != 0.05 {
		t.Errorf("bf.Getfpr() = %f; want 0.05", got)
	}
}

func TestGetNumBitsInFilter(t *testing.T) {
	bf := NewBloomFilter(100, 0.05, "temp", "temp")

	got := bf.GetNumBitsInFilter()
	if got != 624 {
		t.Errorf("bf.GetNumBitsInFilter() = %d; want 624", got)
	}
}

func TestGetNumHashFunctions(t *testing.T) {
	bf := NewBloomFilter(100, 0.05, "temp", "temp")

	got := bf.GetNumHashFunctions()
	if got != 4 {
		t.Errorf("bf.GetNumHashFunctions() = %d; want 4", got)
	}
}

func TestCalcNumBitsInBloomFilter(t *testing.T) {
	got := CalcNumBitsInBloomFilter(100, 0.05)
	if got != 624 {
		t.Errorf("calcNumBitsInBloomFilter(100, 0.05) = %d; want 624", got)
	}
}

func TestCalcNumHashFunctions(t *testing.T) {
	got := CalcNumHashFunctions(624, 100)
	if got != 4 {
		t.Errorf("calcNumHashFunctions(624, 100) = %d; want 4", got)
	}
}

func TestWriteToFile(t *testing.T) {
	bf := NewBloomFilter(100, 0.05, "temp", "temp")

	bf.WriteToFile()

	if !common.FileCheck(common.Inter(os.Getwd())[0].(string) + "/temp") {
		t.Errorf("expected " + common.Inter(os.Getwd())[0].(string) + "/temp" + " to exist, it does not")
	} else {
		os.Remove(common.Inter(os.Getwd())[0].(string) + "/temp")
	}
}

func TestAddToBloomFilter(t *testing.T) {
	bf := NewBloomFilter(100, 0.05, "temp", "temp")

	bf.AddToBloomFilter("orange")

	h1, h2 := murmur3.Sum128([]byte("orange"))
	for i := 0; i < bf.numHashFunctions; i++ {
		h1 += uint64(bf.numHashFunctions) * h2
		if bf.GetBitValue(int(h1%uint64(bf.numBitsInFilter))) == 0 {
			t.Errorf("expected index %d to have a value of 1, instead it has a value of 0", int(h1%uint64(bf.numBitsInFilter)))
		}
	}
}

func TestProbablyExists(t *testing.T) {
	bf := NewBloomFilter(100, 0.05, "temp", "temp")

	bf.AddToBloomFilter("orange")

	if !bf.ProbablyExists("orange") {
		t.Errorf("expected bloomfilter to probably contain orange, it does not")
	}

	if bf.ProbablyExists("ostrich") {
		t.Errorf("expected bloomfilter to not contain ostrich, it does")
	}

}
