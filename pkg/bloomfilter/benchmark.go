package bloomfilter

import (
	"fmt"
	"strconv"
	"time"

	"bitbucket.org/JacobFerrier/racco/pkg/common"
	"github.com/bndr/gotabulate"
)

// BenchmarkSingle set of input values (numItemsInFilter and fpr)
func BenchmarkSingle(numItemsInFilter int, fpr float64, runs int, length int) {
	bf := NewBloomFilter(numItemsInFilter, fpr, "temp", "temp")

	infoTable := gotabulate.Create([][]interface{}{
		[]interface{}{"Number of items in BloomFilter", bf.GetNumItemsInFilter()},
		[]interface{}{"False positive rate of BloomFilter", bf.Getfpr()},
		[]interface{}{"Number of bits in BloomFilter", bf.GetNumBitsInFilter()},
		[]interface{}{"Number of hash functions used in BloomFilter construction", bf.GetNumHashFunctions()},
		[]interface{}{"Number of extra end bits", bf.GetNumExtraEndBits()},
		[]interface{}{"Number of runs to get temporal data", runs},
		[]interface{}{"Length of strings used for AddToBloomFilter and ProbablyExists", length},
	})

	infoTable.SetHeaders([]string{"Item", "Dimension"})
	infoTable.SetTitle("Information about BloomFilter and test quantifiers")
	infoTable.SetAlign("left")

	fmt.Println(infoTable.Render("grid"))

	temporalTable := gotabulate.Create([][]interface{}{
		[]interface{}{"NewBloomFilter", strconv.FormatFloat(timeInitilization(numItemsInFilter, fpr, 10), 'f', -1, 64)},
		[]interface{}{"GetBytes", strconv.FormatFloat(bf.timeGetBytes(runs), 'f', -1, 64)},
		[]interface{}{"GetNumItemsInFilter", strconv.FormatFloat(bf.timeGetNumItemsInFilter(runs), 'f', -1, 64)},
		[]interface{}{"Getfpr", strconv.FormatFloat(bf.timeGetfpr(runs), 'f', -1, 64)},
		[]interface{}{"GetNumBitsInFilter", strconv.FormatFloat(bf.timeGetNumBitsInFilter(runs), 'f', -1, 64)},
		[]interface{}{"GetNumHashFunctions", strconv.FormatFloat(bf.timeGetNumHashFunctions(runs), 'f', -1, 64)},
		[]interface{}{"GetNumExtraEndBits", strconv.FormatFloat(bf.timeGetNumExtraEndBits(runs), 'f', -1, 64)},
		[]interface{}{"calcNumBitsInBloomFilter", strconv.FormatFloat(timeCalcNumBitsInBloomFilter(numItemsInFilter, fpr, runs), 'f', -1, 64)},
		[]interface{}{"calcNumHashFunctions", strconv.FormatFloat(timeCalcNumHashFunctions(numItemsInFilter, fpr, runs), 'f', -1, 64)},
		[]interface{}{"AddToBloomFilter (for a " + strconv.Itoa(length) + "-string)", strconv.FormatFloat(bf.timeAddToBloomFilter(length, runs), 'f', -1, 64)},
		[]interface{}{"ProbablyExists (for a " + strconv.Itoa(length) + "-string)", strconv.FormatFloat(bf.timeProbablyExists(length, runs), 'f', -1, 64)},
	})

	temporalTable.SetHeaders([]string{"Function", "Average time (in nanoseconds) over " + strconv.Itoa(runs) + " runs"})
	temporalTable.SetTitle("Temporal statistics for various BloomFilter operations")
	temporalTable.SetAlign("left")

	fmt.Println(temporalTable.Render("grid"))
}

func timeInitilization(numItemsInFilter int, fpr float64, runs int) float64 {
	var total time.Duration
	for i := 0; i < runs; i++ {
		start := time.Now()
		NewBloomFilter(numItemsInFilter, fpr, "temp", "temp")
		total += time.Since(start)
	}

	return float64(total.Nanoseconds()) / float64(runs)
}

func (bf *BloomFilter) timeGetBytes(runs int) float64 {
	var total time.Duration
	for i := 0; i < runs; i++ {
		start := time.Now()
		bf.GetBytes()
		total += time.Since(start)
	}

	return float64(total.Nanoseconds()) / float64(runs)
}

func (bf *BloomFilter) timeGetNumItemsInFilter(runs int) float64 {
	var total time.Duration
	for i := 0; i < runs; i++ {
		start := time.Now()
		bf.GetNumItemsInFilter()
		total += time.Since(start)
	}

	return float64(total.Nanoseconds()) / float64(runs)
}

func (bf *BloomFilter) timeGetfpr(runs int) float64 {
	var total time.Duration
	for i := 0; i < runs; i++ {
		start := time.Now()
		bf.Getfpr()
		total += time.Since(start)
	}

	return float64(total.Nanoseconds()) / float64(runs)
}

func (bf *BloomFilter) timeGetNumBitsInFilter(runs int) float64 {
	var total time.Duration
	for i := 0; i < runs; i++ {
		start := time.Now()
		bf.GetNumBitsInFilter()
		total += time.Since(start)
	}

	return float64(total.Nanoseconds()) / float64(runs)
}

func (bf *BloomFilter) timeGetNumHashFunctions(runs int) float64 {
	var total time.Duration
	for i := 0; i < runs; i++ {
		start := time.Now()
		bf.GetNumHashFunctions()
		total += time.Since(start)
	}

	return float64(total.Nanoseconds()) / float64(runs)
}

func (bf *BloomFilter) timeGetNumExtraEndBits(runs int) float64 {
	var total time.Duration
	for i := 0; i < runs; i++ {
		start := time.Now()
		bf.GetNumExtraEndBits()
		total += time.Since(start)
	}

	return float64(total.Nanoseconds()) / float64(runs)
}

func timeCalcNumBitsInBloomFilter(numItemsInFilter int, fpr float64, runs int) float64 {
	var total time.Duration
	for i := 0; i < runs; i++ {
		start := time.Now()
		CalcNumBitsInBloomFilter(numItemsInFilter, fpr)
		total += time.Since(start)
	}

	return float64(total.Nanoseconds()) / float64(runs)
}

func timeCalcNumHashFunctions(numItemsInFilter int, fpr float64, runs int) float64 {
	numBitsInFilter := CalcNumBitsInBloomFilter(numItemsInFilter, fpr)

	var total time.Duration
	for i := 0; i < runs; i++ {
		start := time.Now()
		CalcNumHashFunctions(numBitsInFilter, numItemsInFilter)
		total += time.Since(start)
	}

	return float64(total.Nanoseconds()) / float64(runs)
}

func (bf *BloomFilter) timeAddToBloomFilter(length int, runs int) float64 {
	s := common.GenerateRandomString(length)

	var total time.Duration
	for i := 0; i < runs; i++ {
		start := time.Now()
		bf.AddToBloomFilter(s)
		total += time.Since(start)
	}

	return float64(total.Nanoseconds()) / float64(runs)
}

func (bf *BloomFilter) timeProbablyExists(length int, runs int) float64 {
	s := common.GenerateRandomString(length)

	var total time.Duration
	for i := 0; i < runs; i++ {
		start := time.Now()
		bf.ProbablyExists(s)
		total += time.Since(start)
	}

	return float64(total.Nanoseconds()) / float64(runs)
}
