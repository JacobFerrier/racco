package bloomfilter

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"math"
	"math/bits"
	"regexp"
	"strconv"
	"strings"

	"bitbucket.org/JacobFerrier/racco/pkg/common"
	"bitbucket.org/JacobFerrier/racco/pkg/dataset"
	"github.com/spaolacci/murmur3"
)

////
// BloomFilter construction methods, can either build a new BloomFilter or parse in a already constructed BloomFilter from a file
////

// Bytemap object
type Bytemap []byte

// BloomFilter object
type BloomFilter struct {
	bytes            Bytemap
	numItemsInFilter int
	fpr              float64
	numBitsInFilter  int
	numHashFunctions int
	numExtraEndBits  int
	group            string
	name             string
}

// NewBloomFilter builds a new BloomFilter object given the number of items, false positive rate (fpr), a group and a name
func NewBloomFilter(numItemsInFilter int, fpr float64, group string, name string) BloomFilter {
	numBitsInFilter := CalcNumBitsInBloomFilter(numItemsInFilter, fpr)
	numHashFunctions := CalcNumHashFunctions(numBitsInFilter, numItemsInFilter)

	var bytes Bytemap
	bytes = make([]byte, int(math.Ceil(float64(numBitsInFilter)/float64(8))))
	numExtraEndBits := len(bytes)*8 - numBitsInFilter

	return BloomFilter{bytes, numItemsInFilter, fpr, numBitsInFilter, numHashFunctions, numExtraEndBits, group, name}
}

// ConstructFromFile takes the filename of a BloomFilter and parses it in as a BloomFilter object
func ConstructFromFile(filename string) BloomFilter {
	contents, err := ioutil.ReadFile(filename)
	common.Check(err)

	numItemsInFilter, err := strconv.Atoi(strings.Split(string(bytes.Split(contents, []byte("\n"))[0]), ": ")[1])
	common.Check(err)
	fpr, err := strconv.ParseFloat(strings.Split(string(bytes.Split(contents, []byte("\n"))[1]), ": ")[1], 64)
	common.Check(err)
	numBitsInFilter, err := strconv.Atoi(strings.Split(string(bytes.Split(contents, []byte("\n"))[2]), ": ")[1])
	common.Check(err)
	numHashFunctions, err := strconv.Atoi(strings.Split(string(bytes.Split(contents, []byte("\n"))[3]), ": ")[1])
	common.Check(err)
	numExtraEndBits, err := strconv.Atoi(strings.Split(string(bytes.Split(contents, []byte("\n"))[4]), ": ")[1])
	common.Check(err)
	bitString := strings.ReplaceAll(string(bytes.Split(contents, []byte("\n"))[6]), " ", "")

	var bytes Bytemap
	bytes = make([]byte, int(math.Ceil(float64(numBitsInFilter)/float64(8))))

	fullName := strings.SplitAfter(filename, "/data/profiles/")[1]
	r, _ := regexp.Compile("([0-9]+).profile")
	name := r.FindString(fullName)
	group := strings.ReplaceAll(fullName, name, "")[:len(strings.ReplaceAll(fullName, name, ""))-1]

	bf := BloomFilter{bytes, numItemsInFilter, fpr, numBitsInFilter, numHashFunctions, numExtraEndBits, group, name}

	for i := 0; i < len(bitString); i++ {
		if common.Inter(strconv.Atoi(string(bitString[i])))[0] == 1 {
			bf.SetBitToTrue(i)
		}
	}

	return bf
}

// ConstructFromDataset takes in a Dataset and false positive rate (fpr) and returns a BloomFilter from it
func ConstructFromDataset(d dataset.Dataset, fpr float64) BloomFilter {
	bf := NewBloomFilter(d.GetNumElements(), fpr, d.GetGroup(), strings.ReplaceAll(d.GetName(), ".dataset", ".profile"))

	for e := range d.GetElements() {
		bf.AddToBloomFilter(e)
	}

	return bf
}

////
// Setters
////

// SetBytes public setter for bytes
func (bf *BloomFilter) SetBytes(bytes Bytemap) {
	bf.bytes = bytes
}

// SetNumItemsInFilter public setter for numItemsInFilter
func (bf *BloomFilter) SetNumItemsInFilter(numItemsInFilter int) {
	bf.numItemsInFilter = numItemsInFilter
}

// Setfpr public setter for fpr
func (bf *BloomFilter) Setfpr(fpr float64) {
	bf.fpr = fpr
}

// SetNumBitsInFilter public setter for numBitsInFilter
func (bf *BloomFilter) SetNumBitsInFilter(numBitsInFilter int) {
	bf.numBitsInFilter = numBitsInFilter
}

// SetNumHashFunctions public setter for numHashFunctions
func (bf *BloomFilter) SetNumHashFunctions(numHashFunctions int) {
	bf.numHashFunctions = numHashFunctions
}

// SetNumExtraEndBits public setter for numExtraEndBits
func (bf *BloomFilter) SetNumExtraEndBits(numExtraEndBits int) {
	bf.numExtraEndBits = numExtraEndBits
}

// SetGroup public setter for group
func (bf *BloomFilter) SetGroup(group string) {
	bf.group = group
}

// SetName public setter for name
func (bf *BloomFilter) SetName(name string) {
	bf.name = name
}

////
// Getters
////

// GetBytes public getter for bytes
func (bf *BloomFilter) GetBytes() Bytemap {
	return bf.bytes
}

// GetNumItemsInFilter public getter for numItemsInFilter
func (bf *BloomFilter) GetNumItemsInFilter() int {
	return bf.numItemsInFilter
}

// Getfpr public getter for fpr
func (bf *BloomFilter) Getfpr() float64 {
	return bf.fpr
}

// GetNumBitsInFilter public getter for numBitsInFilter
func (bf *BloomFilter) GetNumBitsInFilter() int {
	return bf.numBitsInFilter
}

// GetNumHashFunctions public getter for numHashFunctions
func (bf *BloomFilter) GetNumHashFunctions() int {
	return bf.numHashFunctions
}

// GetNumExtraEndBits public getter for numExtraEndBits
func (bf *BloomFilter) GetNumExtraEndBits() int {
	return bf.numExtraEndBits
}

// GetGroup public getter for group
func (bf *BloomFilter) GetGroup() string {
	return bf.group
}

// GetName public getter for name
func (bf *BloomFilter) GetName() string {
	return bf.name
}

// GetFullName returns full name of BloomFilter
func (bf *BloomFilter) GetFullName() string {
	return bf.group + "/" + bf.name
}

////
// BloomFilter operation helper methods
// calculation formulas provided here: https://hur.st/bloomfilter/
////

// CalcNumBitsInBloomFilter calculates the appropriate number of bits for a filter
func CalcNumBitsInBloomFilter(numItemsInFilter int, fpr float64) int {
	return int(math.Ceil((-1.0 * float64(numItemsInFilter) * math.Log(fpr)) / (math.Pow(math.Log(2), 2))))
}

// CalcNumHashFunctions calculates the appropriate number of hash functions for a filter
func CalcNumHashFunctions(numBitsInFilter int, numItemsInFilter int) int {
	return int(math.Round((float64(numBitsInFilter) / float64(numItemsInFilter)) * math.Log(2)))
}

// Calcfpr calcuates the resultant fpr for a filter
func Calcfpr(numItemsInFilter int, numBitsInFilter int, numHashFunctions int) float64 {
	return math.Pow(1-math.Exp(-1.0*float64(numHashFunctions)/(float64(numBitsInFilter)/float64(numItemsInFilter))), float64(numHashFunctions))
}

// AddToBloomFilter a passed in string
func (bf *BloomFilter) AddToBloomFilter(s string) {
	h1, h2 := murmur3.Sum128([]byte(s))
	for i := 0; i < bf.numHashFunctions; i++ {
		h1 += uint64(bf.numHashFunctions) * h2
		bf.SetBitToTrue(int(h1 % uint64(bf.numBitsInFilter)))
	}
}

// ProbablyExists tests if a string passed in probably exists in the filter
func (bf *BloomFilter) ProbablyExists(s string) bool {
	returnVal := true
	h1, h2 := murmur3.Sum128([]byte(s))
	for i := 0; i < bf.numHashFunctions; i++ {
		h1 += uint64(bf.numHashFunctions) * h2
		if bf.GetBitValue(int(h1%uint64(bf.numBitsInFilter))) == 0 {
			returnVal = false
		}
	}

	return returnVal
}

// SetBitToTrue sets the bit value at a position to be true
func (bf *BloomFilter) SetBitToTrue(position int) {
	which := position >> 3      //which byte to look in
	pos := position & 7         //relative position of the bit within the byte
	val := 1 << uint(8-(pos)-1) //value to 'or' with byte value
	bf.bytes[which] = bf.bytes[which] | byte(val)
}

// GetBitValue returns the bit value at a position in bytes
func (bf *BloomFilter) GetBitValue(position int) byte {
	which := position >> 3      //which byte to look in
	pos := position & 7         //relative position of the bit within the byte
	val := 1 << uint(8-(pos)-1) //value to 'or' with byte value

	return ((bf.bytes[which] & byte(val)) / byte(val))
}

// GetNumOnesInFilter returns the number of ones in the bloomfilter
func (bf *BloomFilter) GetNumOnesInFilter() int {
	count := 0
	for i := 0; i < bf.numBitsInFilter/8; i++ {
		count += bits.OnesCount(uint(bf.GetBytes()[i]))
	}

	return count
}

// MergeBloomFilters bf and bf1
func (bf *BloomFilter) MergeBloomFilters(bf1 BloomFilter, group string, name string) BloomFilter {
	newBloomFilter := BloomFilter{Bytemap{}, bf.numItemsInFilter + bf1.numItemsInFilter, 0.0, bf.numBitsInFilter, bf.numHashFunctions, 0, group, name}

	newBloomFilter.bytes = MergeBytemaps(bf.bytes, bf.bytes)
	newBloomFilter.fpr = Calcfpr(newBloomFilter.numItemsInFilter, newBloomFilter.numBitsInFilter, newBloomFilter.numHashFunctions)
	newBloomFilter.numExtraEndBits = int(math.Min(float64(bf.numExtraEndBits), float64(bf1.numExtraEndBits)))

	return newBloomFilter
}

// MergeBytemaps bytes and bytes1
func MergeBytemaps(bytes Bytemap, bytes1 Bytemap) Bytemap {
	newBytes := Bytemap{}
	for i := 0; i < len(bytes1); i++ {
		orResult := bytes[i] | bytes1[i]
		newBytes = append(newBytes, orResult)
	}

	return newBytes
}

// PrintBits for a given Bytemap
func (bytes Bytemap) PrintBits() {
	for i := 0; i < len(bytes); i++ {
		fmt.Printf("%08b ", bytes[i])
	}
	fmt.Println()
}

////
// BloomFilter file writing methods
////

// WriteToFile writes out a BloomFilter to the structured project location given it's group and name
func (bf *BloomFilter) WriteToFile() {
	out := ("#number of items in filter: " + strconv.Itoa(bf.numItemsInFilter)) + "\n" +
		("#false positive rate of filter: " + fmt.Sprintf("%g", bf.fpr)) + "\n" +
		("#number of bits in filter: " + strconv.Itoa(bf.numBitsInFilter)) + "\n" +
		("#number of hash functions for filter: " + strconv.Itoa(bf.numHashFunctions)) + "\n" +
		("#number of extra end bits for filter: " + strconv.Itoa(bf.numExtraEndBits)) + "\n" +
		("#bytes:") + "\n"

	for i := 0; i < len(bf.bytes); i++ {
		out += fmt.Sprintf("%08b ", bf.bytes[i])
	}

	err := ioutil.WriteFile(common.GetMainDir()+"/data/profiles"+"/"+bf.group+"/"+bf.name, []byte(out), 0644)
	common.Check(err)
}

// WriteToNamedFile writes out a BloomFilter to a file specified by filename
func (bf *BloomFilter) WriteToNamedFile(filename string) {
	out := ("#number of items in filter: " + strconv.Itoa(bf.numItemsInFilter)) + "\n" +
		("#false positive rate of filter: " + fmt.Sprintf("%g", bf.fpr)) + "\n" +
		("#number of bits in filter: " + strconv.Itoa(bf.numBitsInFilter)) + "\n" +
		("#number of hash functions for filter: " + strconv.Itoa(bf.numHashFunctions)) + "\n" +
		("#number of extra end bits for filter: " + strconv.Itoa(bf.numExtraEndBits)) + "\n" +
		("#bytes:") + "\n"

	for i := 0; i < len(bf.bytes); i++ {
		out += fmt.Sprintf("%08b ", bf.bytes[i])
	}

	err := ioutil.WriteFile(filename, []byte(out), 0644)
	common.Check(err)
}

// Print BloomFilter to console
func (bf *BloomFilter) Print() {
	fmt.Printf("BloomFilter name: %s\n", bf.name)
	fmt.Printf("BloomFilter group: %s\n", bf.group)
	fmt.Printf("BloomFilter fullname: %s\n", bf.GetFullName())
	fmt.Printf("BloomFilter numBitsInFilter: %v\n", bf.numBitsInFilter)
	fmt.Printf("BloomFilter numHashFunctions: %v\n", bf.numHashFunctions)
	fmt.Printf("BloomFilter numItemsInFilter: %v\n", bf.numItemsInFilter)
	fmt.Printf("BloomFilter fpr: %v\n", bf.fpr)
	fmt.Printf("BloomFilter numExtraEndBits: %v\n", bf.numExtraEndBits)
	fmt.Printf("BloomFilter bytemap: %v\n", bf.bytes)
}
