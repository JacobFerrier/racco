package common

import (
	"log"
	"math/rand"
	"os"
	"strings"
)

// Check existence of error, exits if error found
func Check(e error) {
	if e != nil {
		log.Fatal(e)
	}
}

// Inter unpacks an interface into slice
func Inter(a ...interface{}) []interface{} {
	return a
}

// DirCheck if a dir exists, if not, creates it
func DirCheck(dir string) {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		Check(os.Mkdir(dir, 0777))
	}
}

// FileCheck if a file exists, returns exsistance status
func FileCheck(filename string) bool {
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		return false
	}

	return true
}

// GenerateRandomPositiveInteger in [1,max]
func GenerateRandomPositiveInteger(max int) int64 {
	/* Cryptographic-Random Number Generation
	nBig, err := rand.Int(rand.Reader, big.NewInt(int64(max)))
	Check(err)

	return nBig.Int64() + 1
	*/

	return rand.Int63n(int64(max)) + 1
}

// GenerateRandomNonNegativeInteger in [0,max]
func GenerateRandomNonNegativeInteger(max int) int64 {
	/* Cryptographic-Random Number Generation
	nBig, err := rand.Int(rand.Reader, big.NewInt(int64(max+1)))
	Check(err)

	return nBig.Int64()
	*/

	return rand.Int63n(int64(max + 1))
}

//GenerateRandomString given a length
func GenerateRandomString(length int) string {
	characters := "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

	var s string
	for i := 0; i < length; i++ {
		s += string(characters[GenerateRandomNonNegativeInteger(len(characters)-1)])
	}

	return s
}

//GetMainDir of project
func GetMainDir() string {
	return strings.SplitAfter(Inter(os.Getwd())[0].(string), "racco")[0]
}
