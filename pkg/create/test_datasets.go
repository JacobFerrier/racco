package create

import (
	"fmt"

	"bitbucket.org/JacobFerrier/racco/pkg/common"
	"bitbucket.org/JacobFerrier/racco/pkg/dataset"
)

func createRandomElement(maxLength int) string {
	return common.GenerateRandomString(int(common.GenerateRandomPositiveInteger(maxLength)))
}

// TestDatasets creates and writes Datasets used for testing
func TestDatasets(setName string, numSets int, elementCount int, elementMaxLength int, writeOver bool) {
	destination := common.GetMainDir() + "/data/datasets/" + setName + "/"
	common.DirCheck(destination)

	for i := 0; i < numSets; i++ {
		if common.FileCheck(destination+fmt.Sprintf("%04d", i)+".dataset") && !writeOver {
			fmt.Println(destination + fmt.Sprintf("%04d", i) + ".dataset" + " exists, passing")
		} else {
			fmt.Println("Creating " + destination + fmt.Sprintf("%04d", i) + ".dataset")
			var stringSlice []string
			for j := 0; j < elementCount; j++ {
				stringSlice = append(stringSlice, createRandomElement(elementMaxLength))
			}

			d := dataset.NewDataset(stringSlice, setName, fmt.Sprintf("%04d", i)+".dataset")
			d.WriteToFile()
		}
	}
}
