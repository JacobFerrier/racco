package create

import (
	"fmt"
	"io/ioutil"
	"strings"

	"bitbucket.org/JacobFerrier/racco/pkg/bloomfilter"
	"bitbucket.org/JacobFerrier/racco/pkg/common"
	"bitbucket.org/JacobFerrier/racco/pkg/dataset"
)

// BloomFilterProfiles creates and writes profiles given a directorty of Datasets to create profiles for
func BloomFilterProfiles(setName string, fpr float64, writeOver bool) {
	destination := common.GetMainDir() + "/data/profiles/" + setName + "/"
	common.DirCheck(destination)

	files, err := ioutil.ReadDir(common.GetMainDir() + "/data/datasets/" + setName + "/")
	common.Check(err)

	for _, file := range files {
		if !strings.HasPrefix(file.Name(), ".") {
			if common.FileCheck(destination+strings.ReplaceAll(file.Name(), ".dataset", ".profile")) && !writeOver {
				fmt.Println(destination + strings.ReplaceAll(file.Name(), ".dataset", ".profile") + " exists, passing")
			} else {
				fmt.Println("Creating " + destination + strings.ReplaceAll(file.Name(), ".dataset", ".profile"))
				d := dataset.ConstructFromFile(common.GetMainDir() + "/data/datasets/" + setName + "/" + file.Name())
				bf := bloomfilter.ConstructFromDataset(d, fpr)
				bf.WriteToFile()
			}
		}
	}

}
